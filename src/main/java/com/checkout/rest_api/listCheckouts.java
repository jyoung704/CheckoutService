package com.checkout.rest_api;

import com.mongodb.*;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Projections;
import org.bson.Document;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/checkouts", method = {RequestMethod.GET, RequestMethod.POST})
@CrossOrigin(origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST})
public class listCheckouts {

    private CheckoutRepository checkoutRepository;
    private MongoClient mongoClient;
    private String mongoDatabaseName = "checkoutDb";
    private String checkoutsCollectionName = "checkouts";
    private double totalWeight = 0;
    private double weightTaken = 0;
    private LocalDate date1;
    private LocalDate date2;

    public listCheckouts(CheckoutRepository checkoutRepository) {
        mongoClient = new MongoClient("mongo", 27017);
        this.checkoutRepository = checkoutRepository;
    }

    @GetMapping("/all")
    public List<checkoutDb> getAll() {
        return checkoutRepository.findAll();
    }

    @GetMapping("/CheckoutList")
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    public ArrayList<Object> getCheckoutList() {

        ArrayList<Object> list = new ArrayList<Object>();
        BasicDBObject query = new BasicDBObject();
        MongoDatabase db = mongoClient.getDatabase(mongoDatabaseName);
        MongoCollection coll = db.getCollection(checkoutsCollectionName);
        MongoCursor cursor = coll.find(query).cursor();

        while (cursor.hasNext()) {
            list.add(cursor.next());
        }
        cursor.close();
        return list;
    }

    @GetMapping("/weight/{startDate}/{endDate}")
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    public double getTotalWeight(@PathVariable String startDate, @PathVariable String endDate) {

        totalWeight = 0;

        date1 = LocalDate.parse(startDate);
        date2 = LocalDate.parse(endDate);

        ArrayList<Object> list = new ArrayList<Object>();
        BasicDBObject query = new BasicDBObject();
        MongoDatabase db = mongoClient.getDatabase(mongoDatabaseName);
        MongoCollection coll = db.getCollection(checkoutsCollectionName);

        query.put("date", BasicDBObjectBuilder.start("$gt", date1).add("$lt", date2).get());

        //inconsistency fix - we want to change userId in the MongoDb to studentId
        MongoCursor cursor = coll.find(query)
                .projection(Projections.exclude("_id", "userId", "date")).cursor();
        int i = 0;
        while (cursor.hasNext()) {
            list.add(cursor.next());
            String[] str = list.get(i).toString().substring(17).split("}", 2);
            totalWeight += Double.valueOf(str[0]);
            i++;
        }

        cursor.close();
        return totalWeight;
    }

    @PostMapping("/weight-taken-this-week")
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    public ResponseEntity getUsersWeeklyWeight(@RequestBody GuestRemWeightCheck remWeightCheck) {

        weightTaken = 0;
        System.out.println(remWeightCheck.toString());

        date1 = LocalDate.parse(remWeightCheck.getStartDate());
        date2 = LocalDate.parse(remWeightCheck.getEndDate());

        ArrayList<Object> list = new ArrayList<Object>();

        BasicDBObject query = new BasicDBObject();
        MongoDatabase db = mongoClient.getDatabase(mongoDatabaseName);
        MongoCollection coll = db.getCollection(checkoutsCollectionName);

        query.put("date", BasicDBObjectBuilder.start("$gt", date1).add("$lt", date2).get());
        query.put("userId", remWeightCheck.getStudentID());

        MongoCursor cursor = coll.find(query)
                .projection(Projections.exclude("_id", "userId", "date")).cursor();
        int i = 0;
        while (cursor.hasNext()) {
            list.add(cursor.next());
            String[] str = list.get(i).toString().substring(17).split("}", 2);
            weightTaken += Double.valueOf(str[0]);
            i++;
        }

        cursor.close();

        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @GetMapping("/collect-taken-weight")
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    public ResponseEntity<Double> collectTakenWeight() {
        System.out.println(weightTaken + "stuff");
        return new ResponseEntity<Double>(weightTaken, HttpStatus.OK);
    }

    @PostMapping("/log-transaction/{userId}/{weight}")
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    public void addCheckout(@PathVariable String userId, @PathVariable double weight) {
        MongoDatabase db = mongoClient.getDatabase(mongoDatabaseName);
        MongoCollection coll = db.getCollection(checkoutsCollectionName);
        Document checkout = new Document()
                .append("userId", userId)
                .append("weight", weight)
                .append("date", LocalDate.now());

        coll.insertOne(checkout);
    }
}
