package com.checkout.rest_api;

public class GuestRemWeightCheck {
    private String studentID;
    private String startDate;
    private String endDate;

    public GuestRemWeightCheck(String studentID, String startDate, String endDate) {
        this.studentID = studentID;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public String getStudentID() {
        return studentID;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setUserId(String studentID) {
        this.studentID = studentID;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "Student [ID=" + studentID + ", Date=" + startDate + " " + endDate + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        GuestRemWeightCheck other = (GuestRemWeightCheck) obj;
        if (studentID != other.studentID)
            return false;
        return true;
    }
}